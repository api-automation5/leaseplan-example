# Lease plan Search API Automation

## Introduction

-----

This is a REST API test solution for endpoint "https://waarkoop-server.herokuapp.com/api/v1/". The published API is the sample Lease Plan test API for assessment.

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven.

## Framework

-----

 - API calls & validations are made using RestAssured and SerenityRest which is a wrapper on top of RestAssured.
 - Test scenarios are written BDD format 
### Refactored Items

-----

1. Updated pom.xml file to enable the build to run successfully.
 - Added dependency for Rest Assured and Cucumber

2. Updated the feature file to include positive and negative Scenarios
3. Parametrized the step definitions to run the scenarios seamless
4. Renamed post_product.feature file to Get_Product to signify the API Service


### Softwares Installation

-----

1. Installation of Java 8 or above from www.oracle.com based on X86/X64 bit systems. I have used Java 11.0.15 for this project.
2. Install Maven 3.5 or higher from https://maven.apache.org. I have used Maven 3.8.6 for this project.
3. Installation of IDE to work with Java. Used Intellij IDE to implement the automation of this project.



### Executing the project

-----

#### Configuration for executing the project in local
- Used maven central repository for running the project
#### Configuration for executing the project in pipeline
- Installed Maven in Gitlab runner. Pulled the docker image over Gitlab CI while build.
- Maven repository is cached to be used every re-execution

-- To clean and install the packages to your local repository
   
    mvn clean install

-- To clean and run all the tests
   
    mvn clean verify

-- To clean and run the test with respect to tags
   
    mvn clean verify -Dtags="tagname"

### Report Verification

-----

After successful build run the below command to view the test results

    open target/site/serenity/index.html

The report records the count of both passed and failed case counts in graph

![img.png](img.png)

Also the report records the API Calls and its response in a very readable format as below

![img_1.png](img_1.png)

### Writing/Updating Test Scenarios

-----

1. Start writing new test scenarios in test-> resources -> features folder
   - New functionalities gets a new folder
   - Adding feature to existing functionality - Can be done by adding feature file to the existing folder
   - Adding scenarios to existing feature file -> Open the feature file and add the scenario
2. For Adding a new scenario follow the below steps
   - Start by adding valid tags. Keep in mind to add the service name and common tags which are relevant
   - Start writing the Scenario description under Scenario:
   - If there are examples involved write the description under Scenario Outline:
   - Write the BDD with format Given, When, Then and And
   - If parameters passed in the steps, provide with the format <parameter1>
   - Values of the parameters can be added in Examples section with | symbol
   - Write the step definitions for the scenarios added in test -> java -> starter -> stepdefinitions
