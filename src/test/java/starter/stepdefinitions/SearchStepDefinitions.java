package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import static net.serenitybdd.rest.SerenityRest.*;

public class SearchStepDefinitions {

    private String baseUri = "https://waarkoop-server.herokuapp.com/api/v1/";
    private String resource = "search/test/";

    Response Actualresponse;

    @When("he calls endpoint with (.*)")
    public void heCallsEndpoint(String productName) {
        String URL = baseUri+resource+productName;
        SerenityRest.given().get(URL);
    }

    @Then("he sees the results displayed for (.*)")
    public void heSeesTheResultsDisplayedForApple(String expectedProduct) {
        Actualresponse =  lastResponse();
        restAssuredThat(response-> Actualresponse.then().extract().body().jsonPath().getJsonObject("title").toString().contains(expectedProduct));

    }

    @Then("he sees the response of the API is (\\d+)")
    public void heseestheresponseoftheAPI(Integer expectedStatus){
        restAssuredThat(response -> response.statusCode(expectedStatus));
    }

    @Then("he see the response message as (.*)")
    public void heseetheresponsemessage(String expectedMessage){
        Actualresponse = lastResponse();
        restAssuredThat(response -> Actualresponse.then().extract().body().path("detail","message").toString().contains(expectedMessage));
    }

    @When("he calls the endpoint with (.*)")
    public void hecallstheendpointwithpostmethod(String Scenario){
        String URL = baseUri+resource+"cheese";
        if(Scenario.contains("post method")) {
            SerenityRest.given().post(URL);
        } else if (Scenario.contains("query parameter")) {
            SerenityRest.given().queryParam("product","cheese").get(URL);
        } else if (Scenario.contains("no parameter")) {
            URL = baseUri+resource;
            SerenityRest.given().get(URL);
        } else if (Scenario.contains("different product")) {
            SerenityRest.given().get(URL);
        }
    }
}
