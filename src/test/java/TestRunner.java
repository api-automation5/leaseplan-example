import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = "pretty", glue = "starter.stepdefinitions", features = "src/test/resources/features/search/Get_Product.feature")
public class TestRunner {}
