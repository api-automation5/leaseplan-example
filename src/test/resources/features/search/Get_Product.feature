Feature: Search for the product

  @positive @completed @searchapi
  Scenario Outline: Verify search results are successfully retrieved for the given product names
    When he calls endpoint with <productName>
    Then he sees the response of the API is 200
    Then he sees the results displayed for <productName>
    Examples:
    |productName|
    |apple       |
    |mango       |
    |tofu        |
    |water       |

  @negative @completed @searchapi
  Scenario Outline: Verify error message is displayed when different negative scenarios are performed
    When he calls the endpoint with <scenario>
    Then he sees the response of the API is <status code>
    Then he see the response message as <error message>
    Examples:
    |scenario||status code||error message|
    |post method||405|     |Method Not Allowed|
    |no parameter   ||401| |Not authenticated |
    |different product||404||Not Found        |
